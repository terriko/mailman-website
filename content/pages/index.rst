:slug: home
:save_as: index.html

Mailman, the GNU Mailing List Manager
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Mailman is free software for managing electronic mail discussion and
e-newsletter lists. Mailman is integrated with the web, making it easy
for users to manage their accounts and for list owners to administer
their lists. Mailman supports built-in archiving, automatic bounce
processing, content filtering, digest delivery, spam filters, and more.
See the `features page <features.html>`__ for details.

Mailman is free software, distributed under the `GNU General Public
License <http://www.gnu.org/copyleft/gpl.html>`__, and written in
the `Python <http://www.python.org/>`__ programming language.

We want to thank our generous list of
`financial donors <http://wiki.list.org/COM/Donors>`__ whose
contributions allowed us to send a core developer to PyCon 2015.
Please consider
`donating to the GNU Mailman <https://my.fsf.org/civicrm/contribute/transact?reset=1&id=22>`__
project yourself. (`details <http://wiki.list.org/x/R4BJ>`__)

See the `Security page <http://wiki.list.org/SEC/Home>`__ for important
security related information, and the `help <help.html>`__ page for additional
resources for users, list and site administrators, and developers.

Our `wiki <http://wiki.list.org>`__ has lots of other great information,
including a FAQ.

Our `Code of Conduct <coc.html>`__ applies to all of our modes of discussion
and collaboration, including the GNU Mailman mailing lists, wiki pages, and
IRC channels.


Current Version
~~~~~~~~~~~~~~~

The current stable GNU Mailman version are 2.1.20 released on
31-Mar-2015. and 3.0.1 (Show Don't Tell) released on 15-Nov-2015.


Contact Us
~~~~~~~~~~

There are various ways to `get in touch <contact.html>`__ with the GNU Mailman
Steering Committee, our security contacts, and the Mailman developer and user
communities, via IRC and email.


Acknowledgements
~~~~~~~~~~~~~~~~

Mailman's lead developer is `Barry Warsaw <http://barry.warsaw.us>`__. Core
developers are Mark Sapiro, Aurélien Bompard, Florian Fuchs, Terri Oda,
Stephen J. Turnbull, Abhilash Raj. We thank all of our great GSoC students and
mentors, and the wider Mailman community for contributing to the development
of GNU Mailman.

Thanks go out to:

- `Canonical <http://www.canonical.com>`__ for their current support
  and `Zope Corporation <http://www.zope.com>`__ for their past support
  of Barry's work while under their employ.
- `The Mail-Archive.com <http://www.mail-archive.com>`__ for their
  donation kickstarting the `directed GNU Mailman/Free Software
  Foundation donation
  fund <https://my.fsf.org/civicrm/contribute/transact?reset=1&id=22>`__.
- `cPanel <http://cpanel.com>`__ for their generous donation of server
  resources for our `GitLab <http://gitlab.com>`__ CI system.
- the `list of   contributors <http://tinyurl.com/qcb4hob>`__,
  bug hunters, big idea people, and others who have helped immensely
  with Mailman's development.
