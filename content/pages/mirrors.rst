:slug: mirrors
:save_as: mirrors.html

Mailman web page mirrors
~~~~~~~~~~~~~~~~~~~~~~~~

These web pages are mirrored in several locations for your convenience.
Here are the current list of mirrors:

-  `www.list.org <http://www.list.org/>`__
-  `GNU <http://www.gnu.org/software/mailman/index.html>`__
-  `SourceForge <http://mailman.sourceforge.net>`__
-  `mirror.list.org <http://mirror.list.org/>`__
