:slug: contact
:save_as: contact.html

About Us
~~~~~~~~

First, who is "Us"?

GNU Mailman is `free software
<https://www.gnu.org/philosophy/free-sw.html>`__, developed and used by people
from all over the world.  We work on Mailman as volunteers and give it away
for free.  There is no official paid support organization behind Mailman; we
are a community.

That said, there are some official channels for contacting the folks involved
with Mailman.


The GNU Mailman Steering Committee
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We have a steering committee of long-time core developers who collectively
make most of the global project-based decisions.  If you'd like to contact us
please email `mailman-cabal at python dot org
<mailto:%6D%61%69%6C%6D%61%6E%2D%63%61%62%61%6C%40%70%79%74%68%6F%6E%2E%6F%72%67>`__

Security
~~~~~~~~

If you would like to report a security issue, or have other security related
questions, please email `mailman-security at python dot org
<mailto:%6D%61%69%6C%6D%61%6E%2D%73%65%63%75%72%69%74%79%40%70%79%74%68%6F%6E%2E%6F%72%67>`__.

The Community
~~~~~~~~~~~~~

- `Mailman Announce
  <http://mail.python.org/mailman/listinfo/mailman-announce>`__ (`*archives*
  <http://mail.python.org/pipermail/mailman-announce/>`__) is a read-only list
  that you can subscribe to if you are only interested in release notices and
  other important news. Only the core Mailman developers can post messages to
  this list.  This mailing list is also available on the `Gmane
  <http://www.gmane.org>`__ ``gmane.mail.mailman.announce`` newsgroup.

- `Mailman Users <http://mail.python.org/mailman/listinfo/mailman-users>`__
  (`*archives* <http://mail.python.org/pipermail/mailman-users/>`__) is a public
  discussion mailing list for users who need help installing, configuring, or
  running GNU Mailman (especially version 2.1). Email the group at
  `mailman-users at python dot org
  <mailto:%6D%61%69%6C%6D%61%6E%2D%75%73%65%72%73%40%70%79%74%68%6F%6E%2E%6F%72%67>`__.
  This mailing list is also available on the `Gmane <http://www.gmane.org>`__
  gmane.mail.mailman.user newsgroup.

- `Mailman Developers
  <http://mail.python.org/mailman/listinfo/mailman-developers>`__ (`*archives*
  <http://mail.python.org/pipermail/mailman-developers/>`__) is a public
  discussion mailing list for developers who want to work together to improve
  GNU Mailman, and users who are working with Mailman 3.  Email the group at
  `mailman-developers at python dot org
  <mailto:%6D%61%69%6C%6D%61%6E%2D%64%65%76%65%6C%6F%70%65%72%73%40%70%79%74%68%6F%6E%2E%6F%72%67>`__.
  This mailing list is also available on the `Gmane <http://www.gmane.org>`__
  gmane.mail.mailman.devel newsgroup.

- `Mailman Internationalization
  <http://mail.python.org/mailman/listinfo/mailman-i18n>`__ (`*archives*
  <http://mail.python.org/pipermail/mailman-i18n/>`__) is the public mailing
  list for discussing the multi-lingual support in Mailman 2.1.  Everyone who is
  working on translations of Mailman should subscribe to this mailing list. .
  This mailing list is also available on the `Gmane <http://www.gmane.org>`__
  gmane.mail.mailman.i18n newsgroup.

IRC
~~~

Most of the developers, and lots of users, also hang out on the ``#mailman``
channel on the `freenode IRC network <https://freenode.net/>`__.
