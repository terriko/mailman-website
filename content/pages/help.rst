:slug: help
:save_as: help.html

Help
~~~~

There are many resources available for those who need help with Mailman,
beyond the `on-line documentation <docs.html>`__. If you are having
problems installing or configuring Mailman, you should probably start
here:

-  The community driven `Frequently Asked
   Questions <http://wiki.list.org/display/DOC/Frequently+Asked+Questions>`__

You should also check out the community driven
`wiki <http://wiki.list.org>`__, which hold the most up-to-date
information about Mailman.

Beyond those resources, please use the `discussion lists <lists.html>`__
for asking questions. There are lots of very helpful and knowledgeable
people on the lists who can help. Probably the most helpful will be the
`mailman-users <http://mail.python.org/mailman/listinfo/mailman-users>`__
mailing list.
