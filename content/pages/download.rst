:slug: download
:save_as: download.html

Requirements
~~~~~~~~~~~~

See the `requirements page <requirements.html>`__ for the list of
pre-requisites.

Downloading
~~~~~~~~~~~

Mailman 2.1 is available from the following sources:

-  `Launchpad <http://launchpad.net/mailman>`__
-  `GNU <http://ftp.gnu.org/gnu/mailman/>`__

Mailman 3.0 is available from the `Python Package Index (PyPI) <https://pypi.python.org/pypi?%3Aaction=search&term=mailman&submit=search>`__.

*Note: the GNU ftp site may lag behind Launchpad, especially just after
a release announcement. It often takes a little while for the GNU ftp
site to catch up.*

Signing keys
~~~~~~~~~~~~

We always sign releases with the `GPG keys <http://www.gnupg.org>`__ of one of
the core developers: Barry Warsaw or Mark Sapiro.  Our public keys are
available from all the public keyservers. For cross referencing, here are the
keys we use to sign releases:

+--------------------+--------------------+--------------------------------------------------------+
| Developer          | GPG key id         | GPG fingerprint                                        |
+====================+====================+========================================================+
| Barry Warsaw       | ``A74B06BF``       | ``8417 157E DBE7 3D9E AC1E  539B 126E B563 A74B 06BF`` |
+--------------------+--------------------+--------------------------------------------------------+
| Mark Sapiro        | ``953B8693``       | ``C638 CAEF 0AC2 1563 736B  5A22 555B 975E 953B 8693`` |
+--------------------+--------------------+--------------------------------------------------------+
